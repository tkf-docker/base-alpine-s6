def loadConfigYaml()
{
  def valuesYaml = readYaml (file: './config.yaml')
  return valuesYaml;
}

pipeline {
  agent {
    // By default run stuff on a x86_64 node, when we get
    // to the parts where we need to build an image on a diff
    // architecture, we'll run that bit on a diff agent
    label 'X86_64'
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '5', daysToKeepStr: '60'))
    parallelsAlwaysFailFast()
  }

  // Configuration for the variables used for this specific repo
  environment {
    CONTAINER_NAME = 'base-alpine-s6'
    DOCKER_REGISTRY = 'teknofile'
    LOCAL_DOCKER_REGISTRY = 'artifactory.cosprings.teknofile.net'
  }

  stages {
    // Setup all the basic enviornment variables needed for the build
    stage("Setup ENV variables") {
      steps {
        script {
          env.EXIT_STATUS = ''
          env.CURR_DATE = sh(
            script: '''date '+%Y-%m-%d_%H:%M:%S%:z' ''',
            returnStdout: true).trim()
          env.GITHASH_SHORT = sh(
            script: '''git log -1 --format=%h''',
            returnStdout: true).trim()
          env.GITHASH_LONG = sh(
            script: '''git log -1 --format=%H''',
            returnStdout: true).trim()
        }
      }
    }

    stage('Build Containers') {
      steps {
        echo "Running on node: ${NODE_NAME}"
        git([url: 'https://gitlab.com/tkf-docker/base-alpine-s6.git', branch: env.BRANCH_NAME, credentialsId: 'TKFBuildBot-GitLabUser'])

        script {
          configYaml = loadConfigYaml()

          env.ALPINE_VERSION = configYaml.alpine.srcVersion
          env.S6_OVERLAY_VERSION = configYaml.s6overlay.version

          withDockerRegistry(credentialsId: 'teknofile-dockerhub') {
            sh '''
              docker buildx create --bootstrap --use --name tkf-builder --platform linux/amd64,linux/arm64,linux/arm
              docker buildx build \
                --no-cache \
                --pull \
                --build-arg ALPINE_VERSION=${ALPINE_VERSION} \
                --build-arg S6_OVERLAY_VERSION=${S6_OVERLAY_VERSION} \
                --platform linux/amd64,linux/arm64,linux/arm \
                -t ${DOCKER_REGISTRY}/${CONTAINER_NAME}:${ALPINE_VERSION} \
                -t ${DOCKER_REGISTRY}/${CONTAINER_NAME}:latest \
                . \
                --push

              docker pull ${DOCKER_REGISTRY}/${CONTAINER_NAME}:${ALPINE_VERSION}
              docker tag ${DOCKER_REGISTRY}/${CONTAINER_NAME}:${ALPINE_VERSION} ${LOCAL_DOCKER_REGISTRY}/${CONTAINER_NAME}:${ALPINE_VERSION}
              docker tag ${DOCKER_REGISTRY}/${CONTAINER_NAME}:${ALPINE_VERSION} ${LOCAL_DOCKER_REGISTRY}/${CONTAINER_NAME}:latest
            '''
          }
        }
      }
    }
  }
  post {
    cleanup {
      cleanWs()
	    deleteDir()
    }
  }
}